import { createRouter, createWebHistory } from 'vue-router'

import HomeView from '../views/HomeView.vue'
import mainBody from '../views/layout/mainBody.vue'

const routes = [
  {
    path: '/',
    // name: 'home',
    component: mainBody,
    children:[
      {
        path: '',
        redirect: '/home'
      },
      {
        path: '/home',
        component: HomeView,
        name: 'home',
        meta: {
          name: 'Home',
          requireAuth: true
        }
      }
    ]
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
